;    memtest - Dragon 32/64 memory test program
;
;    Copyright (C) 2020  Stewart Orchard  <sorchard001@gmail.com>
;
;    This program is free software: you can redistribute it and/or modify
;    it under the terms of the GNU General Public License as published by
;    the Free Software Foundation, either version 3 of the License, or
;    (at your option) any later version.
;
;    This program is distributed in the hope that it will be useful,
;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;    GNU General Public License for more details.
;
;    You should have received a copy of the GNU General Public License
;    along with this program.  If not, see <http://www.gnu.org/licenses/>.



; Program notes:
; Doesn't use memory for variables or stack (other than video display)
; Position independent code
; Can be run from ROM
; Assemble with asm6809 (www.6809.org.uk/asm6809)


; number of digits in pass counter
COUNTER_DIGITS	equ 6


; branch and link macro
; performs relative subroutine call with return address in s
; subroutine should return with jmp ,s
brl	macro
	leas \1,pcr
	exg s,pc
	endm

; macro that displays text message
show_str macro
	leau \1,pcr
	brl vdu_outstrz
	endm


	org $2000

start_address

	orcc #$50
	leay seq_start,pcr

	brl vdu_cls

	ldx PRM_VDU_ADDR,y
	leau 32,x
	ldd #$3000 + COUNTER_DIGITS
1	sta ,-u
	decb
	bne 1b

	show_str txt_title

mloop
	brl vdu_set_mode
	lda PRM_FUNCTION,y	; function id
	leax fn_table,pcr	; function table
	ldd a,x			; look up function offset
	leax fn_base_addr,pcr	; get function base address ...
	jmp d,x			; ... and jump to offset

; -------------------------------------
; fn_base_addr defines reference point to calculate function address
; actual value doesn't matter other than ideally a small offset from pc
; in function jump calculation

fn_base_addr

; -------------------------------------
; Check if running from ROM
; Attempts to change a memory location within the program image.
; If value changes, then assume running from RAM.
; Otherwise adjust sequence pointer to new point in sequence.

fn_romcheck
	leax mem_test_var,pcr
	lda ,x
	com ,x
	cmpa ,x
	beq 5f  ; value didn't change - probably rom

	show_str txt_ram_resident
	leay 5,y
1	bra mloop

5	show_str txt_rom_resident
	ldd PRM_OFFSET,y
	leay d,y
	bra 1b

; -------------------------------------
; Ram-resident check for 64K memory
; Writes to two locations 32K apart
; If upper address does not contain expected value then 32K assumed
; Stays in map type 1 if 64K detected and new point in sequence.

fn_check64k_ram
	sta $ffdf	; map type 1
	ldd #$55aa
	leax mem_test_var,pcr
	std $8000,x
	coma
	comb
	std ,x
	ldd $8000,x
	subd #$55aa
	bne 1f 		; not 64k (or upper 32K faulty)

	show_str txt_ram64
	ldd PRM_OFFSET,y
	leay d,y
	bra mloop

1	sta $ffde	; map type 0
	show_str txt_ram32
	leay 5,y
	bra mloop

; -------------------------------------
; Rom-resident check for 64K memory

fn_check64k_rom
	ldx PRM_VDU_ADDR,y
	leax 1024,x
	ldd #$55aa
	sta $ffd5	; page 1
	std ,x
	sta $ffd4	; page 0
	coma
	comb
	std ,x

	sta $ffd5	; page 1
	ldd ,x
	sta $ffd4	; page 0
	subd #$55aa
	bne 1f 		; not 64k (or upper 32K faulty)

	show_str txt_ram64
	ldd PRM_OFFSET,y
	leay d,y
	lbra mloop

1	show_str txt_ram32
	leay 5,y
	lbra mloop

; -------------------------------------
; Switch to memory page 0
; Copies the video display in place from page 1
fn_page0
	clrb
	ldx PRM_VDU_ADDR,y
1	sta $ffd5	; page 1
	ldu ,x
	sta $ffd4	; page 0
	stu ,x++
	decb
	bne 1b
	sta $ffd2	; video in lower 32K
	show_str txt_page0
	leay 3,y
	lbra mloop

; Switch to memory page 1
; Copies the video display in place from page 0
fn_page1
	clrb
	ldx PRM_VDU_ADDR,y
1	sta $ffd4	; page 0
	ldu ,x
	sta $ffd5	; page 1
	stu ,x++
	decb
	bne 1b
	sta $ffd3	; video in upper 32K
	show_str txt_page1
	leay 3,y
	lbra mloop

; -------------------------------------
; macro that displays test address range

show_test_range macro
	show_str txt_address
	ldd PRM_TEST_START,y
	brl vdu_outhex16
	lda #'-
	sta ,x+
	ldd PRM_TEST_END,y
	brl vdu_outhex16
	endm

; -------------------------------------
; perform walking bit memory tests

fn_memtest_walk
	show_test_range
	show_str txt_walking1
	brl walking1
	show_str txt_walking0
	brl walking0
	leay 7,y
	lbra mloop

; -------------------------------------
; write pattern to memory

fn_pattern_write
	show_test_range
	show_str txt_pattern_write
	brl ptn_write
	leay 9,y
	lbra mloop

fn_anti_ptn_write
	show_test_range
	show_str txt_anti_ptn_write
	brl anti_ptn_write
	leay 9,y
	lbra mloop

; -------------------------------------
; approx n x 0.5s delay @ 895KHz

fn_delay
	show_str txt_delay
	ldb PRM_DELAY,y
	ldx #55937
1	leax -1,x
	bne 1b
	decb
	bne 1b
	leay 4,y
	lbra mloop

; -------------------------------------
; Check pattern in memory

fn_pattern_check
	show_test_range
	show_str txt_pattern_check
	brl ptn_check
	leay 9,y
	lbra mloop

fn_anti_ptn_check
	show_test_range
	show_str txt_anti_ptn_check
	brl anti_ptn_check
	leay 9,y
	lbra mloop

; -------------------------------------
; copy program and display to new addresses
; jumps into program at new address
;
fn_relocate
	leas start_address,pcr		; copy program
	ldu PRM_NEW_ADDR,y		;
	ldx #end_address-start_address	;
1	lda ,s+				;
	sta ,u+				;
	leax -1,x			;
	bne 1b				;

	tfr s,d		; compute program relocation offset
	comb		; then adjust y to point
	coma		; to relocated params
	addd #1		;
	leau d,u	;
	tfr u,d		;
	leay d,y	;

	lds PRM_VDU_ADDR,y	; copy video display
	ldu PRM_NEW_VDU,y	;
	clrb			;
1	ldx ,s++		;
	stx ,u++		;
	decb			;
	bne 1b			;

	ldx PRM_NEW_ADDR,y
	leay 7,y
	jmp mloop-start_address,x

; -------------------------------------
; copy display to new address
;
fn_relocate_vid
	lds PRM_VDU_ADDR,y
	ldu PRM_NEW_VDU,y
	clrb
1	ldx ,s++
	stx ,u++
	decb
	bne 1b

	leay 5,y
	lbra mloop

; -------------------------------------
; adjust parameter pointer by specified offset
;
fn_seqjump
	ldd PRM_OFFSET,y
	leay d,y
	lbra mloop

; -------------------------------------
; update on-screen counter
; directly manipulates characters in video ram
;
fn_count
	ldx PRM_VDU_ADDR,y
	leax 32,x
	ldb #COUNTER_DIGITS
4	coma	; set carry
5	lda ,-x
	adca #0
	cmpa #'9
	bls 1f
	lda #'0
	sta ,x
	decb
	bne 4b
1	sta ,x
	clra	; clear carry
	decb
	bne 5b

	leay 3,y
	lbra mloop

; -------------------------------------

; walking bit 0
walking0
	ldx PRM_TEST_START,y
1	ldd #$7f7f
2	std ,x
	cmpd ,x
	bne 9f		; error
	lsra
	rorb
	ora #128
	bcs 2b
	leax 2,x
	cmpx PRM_TEST_END,y
	bls 1b
	jmp ,s


; walking bit 1
walking1
	ldx PRM_TEST_START,y
1	ldd #$8080
2	std ,x
	cmpd ,x
	bne 9f		; error
	lsra
	lsrb
	bne 2b
	leax 2,x
	cmpx PRM_TEST_END,y
	bls 1b
	jmp ,s

9	cmpa ,x		; check which address failed
	bne fail	; failed at x
	tfr b,a		; failed at x+1
	leax 1,x
	bra fail


PATTERN_INC	equ 113		; relatively prime to 256

ptn_write
	ldx PRM_TEST_START,y
	ldd PRM_SEED,y
1	sta ,x+
2	addd #PATTERN_INC*257
	beq 2b		; reduce cycle length to 65535
	cmpx PRM_TEST_END,y
	bls 1b
	jmp ,s

anti_ptn_write
	ldx PRM_TEST_START,y
	ldd PRM_SEED,y
1	coma
	sta ,x+
	coma
2	addd #PATTERN_INC*257
	beq 2b		; reduce cycle length to 65535
	cmpx PRM_TEST_END,y
	bls 1b
	jmp ,s

ptn_check
	ldx PRM_TEST_START,y
	ldd PRM_SEED,y
1	cmpa ,x+
	bne 9f		; error
2	addd #PATTERN_INC*257
	beq 2b		; reduce cycle length to 65535
	cmpx PRM_TEST_END,y
	bls 1b
	jmp ,s

anti_ptn_check
	ldx PRM_TEST_START,y
	ldd PRM_SEED,y
1	coma
	cmpa ,x+
	bne 9f		; error
	coma
2	addd #PATTERN_INC*257
	beq 2b		; reduce cycle length to 65535
	cmpx PRM_TEST_END,y
	bls 1b
	jmp ,s

9	leax -1,x	; correct to failed address

; display failure info
; x = address
; a = expected byte

fail
	leau ,x		; save failed address

	ldx PRM_VDU_ADDR,y
	leax 205,x
	brl vdu_outhex8	; display expected byte
	leax 30,x
	lda ,u		;
	brl vdu_outhex8 ; display byte from failed address

	leax 30-96,x
	tfr u,d
	brl vdu_outhex16	; display failed address

	lda $ff22
	eora #8
	sta $ff22

	show_str txt_fail
	bra *			; halt program


; -------------------------------------

; output 16 bit hex value
; d = value
; x = screen address

vdu_outhex16
	leau ,s
	brl vdu_outhex8
	tfr b,a
	leas ,u

; output 8 bit hex value
; a = value
; x = screen address

vdu_outhex8
	tfr a,dp
	lsra
	lsra
	lsra
	lsra
	adda #'0
	cmpa #'9
	bls 1f
	adda #1-10-'0
1	sta ,x+
	tfr dp,a
	anda #15
	adda #'0
	cmpa #'9
	bls 1f
	adda #1-10-'0
1	sta ,x+
	jmp ,s

; display a string
; u points to screen offset followed by
; null terminated string

vdu_outstrz
	ldx PRM_VDU_ADDR,y
	ldd ,u++	; add offset to video start
	leax d,x	;
5	lda ,u+		; get character
	bne 1f		; display if non-zero
	jmp ,s		; else return

1	cmpa #$0d	; check for CR
	bne 1f		;
	exg d,x		; move to start of next line
	andb #$e0	;
	addd #$20	;
	exg d,x		;
	bra 5b		;

1	cmpa #$40	; map char code
	blo 2f		; into 6847 code
	cmpa #$60	;
	blo 1f		;
	eora #$60	;
1	eora #$40	;
2	sta ,x+		;
	bra 5b		;


; clear screen

vdu_cls
	ldx PRM_VDU_ADDR,y
	ldu #$2020
	clrb
1	stu ,x++
	decb
	bne 1b
	jmp ,s


; set display to text mode and setup base address
; video address is restricted to lower 32K

vdu_set_mode
	clra		; set text mode
	sta $ff22	;
	ldx #$ffc6
	sta -6,x	; set 512 byte mem mode
	sta -4,x	;
	sta -2,x	;
	lda PRM_VDU_ADDR,y
	lsra		; set video base address
1	clrb		;
	lsra		;
	rolb		;
	sta b,x		;
	leax 2,x	;
	cmpx #$ffd2	;
	blo 1b		;
	jmp ,s

; -------------------------------------
; strings for display
; first param is position on screen
; followed by null terminated string

txt_title
	fdb 0
	fcb /MEMORY TEST/,0

txt_address
	fdb 64
	fcb /TESTING /,0

txt_pattern_write
	fdb 96
	fcb /PATTERN WRITE      /,0

txt_anti_ptn_write
	fdb 96
	fcb /ANTI-PATTERN WRITE /,0

txt_delay
	fdb 96
	fcb /DELAY              /,0

txt_pattern_check
	fdb 96
	fcb /PATTERN CHECK      /,0

txt_anti_ptn_check
	fdb 96
	fcb /ANTI-PATTERN CHECK /,0

txt_walking0
	fdb 96
	fcb /WALKING ANTI-BIT  /,0

txt_walking1
	fdb 96
	fcb /WALKING BIT       /,0

txt_ram_resident
	fdb 32
	fcb /RAM RESIDENT/,0

txt_rom_resident
	fdb 32
	fcb /ROM RESIDENT/,0

txt_ram32
	fdb 16
	fcb /32K/,0

txt_ram64
	fdb 16
	fcb /64K/,0

txt_page0
	fdb 82
	fcb /LOWER 32K/,0

txt_page1
	fdb 82
	fcb /UPPER 32K/,0


txt_fail
	fdb 160
	fcb /*** FAILED @ /,13
	fcb /*** EXPECTED /,13
	fcb /*** FOUND    /,0

; -------------------------------------

; memory location used for rom-resident check
; and 64K size detect

mem_test_var	fdb $55aa

; -------------------------------------

; table of functions

; macro that creates a jump table entry
; first an EQUate to form an offset into the jump table
; then an address offset to point to the function

fn_entry macro
\1	equ (* - fn_table)
	fdb \2 - fn_base_addr
	endm

fn_table
	fn_entry "ROMCHECK",		fn_romcheck
	fn_entry "CHECK64K_RAM",	fn_check64k_ram
	fn_entry "CHECK64K_ROM",	fn_check64k_rom
	fn_entry "PAGE0",		fn_page0
	fn_entry "PAGE1",		fn_page1
	fn_entry "MEMTEST_WALK",	fn_memtest_walk
	fn_entry "PATTERN_WRITE",	fn_pattern_write
	fn_entry "ANTI_PATTERN_WRITE",	fn_anti_ptn_write
	fn_entry "DELAY",		fn_delay
	fn_entry "PATTERN_CHECK",	fn_pattern_check
	fn_entry "ANTI_PATTERN_CHECK",	fn_anti_ptn_check
	fn_entry "RELOCATE_ALL",	fn_relocate
	fn_entry "RELOCATE_VID",	fn_relocate_vid
	fn_entry "SEQJUMP",		fn_seqjump
	fn_entry "COUNT",		fn_count


; memtest parameters
PRM_FUNCTION	equ 0
PRM_VDU_ADDR	equ 1
PRM_TEST_START	equ 3
PRM_TEST_END	equ 5
PRM_SEED	equ 7

; delay param
PRM_DELAY	equ 3

; relocate params
PRM_NEW_VDU	equ 3
PRM_NEW_ADDR	equ 5

; sequence jump param
PRM_OFFSET	equ 3

; -------------------------------------
; Supporting macros for test sequence

; check if ram or rom resident
rom_check macro
1	fcb ROMCHECK
	fdb \1		; video address
	fdb \2-1b	; jump offset for rom
	endm

; check if 64k present in ram-resident mode
check_64k_ram macro
1	fcb CHECK64K_RAM
	fdb \1		; video address
	fdb \2-1b	; jump offset for 64k
	endm

; check if 64k present in rom-resident mode
check_64k_rom macro
1	fcb CHECK64K_ROM
	fdb \1		; video address
	fdb \2-1b	; jump offset for 64k
	endm

; switch to memory page 0
set_page0 macro
	fcb PAGE0
	fdb \1		; video address
	endm

; switch to memory page 1
set_page1 macro
	fcb PAGE1
	fdb \1		; video address
	endm

; relocate program and video display to new addresses
relocate_all macro
	fcb RELOCATE_ALL
	fdb \1		; video address
	fdb \2		; new video address
	fdb \3		; new program address
	endm

; relocate video display to new address
relocate_vid macro
	fcb RELOCATE_VID
	fdb \1		; video address
	fdb \2		; new video address
	endm

; walking bit memory tests
walking_tests macro
	fcb MEMTEST_WALK
	fdb \1		; video address
	fdb \2		; test start address
	fdb \3		; test end address
	endm

; memory pattern write
pattern_write macro
	fcb PATTERN_WRITE
	fdb \1		; video address
	fdb \2		; test start address
	fdb \3		; test end address
	fdb \4		; seed
	endm

; memory anti-pattern write
anti_ptn_write macro
	fcb ANTI_PATTERN_WRITE
	fdb \1		; video address
	fdb \2		; test start address
	fdb \3		; test end address
	fdb \4		; seed
	endm

; memory pattern check
pattern_check macro
	fcb PATTERN_CHECK
	fdb \1		; video address
	fdb \2		; test start address
	fdb \3		; test end address
	fdb \4		; seed
	endm

; memory anti-pattern check
anti_ptn_check macro
	fcb ANTI_PATTERN_CHECK
	fdb \1		; video address
	fdb \2		; test start address
	fdb \3		; test end address
	fdb \4		; seed
	endm

; approx n x 0.5s delay @ 895KHz
delay_0_5s macro
	fcb DELAY
	fdb \1		; video address
	fcb \2		; delay
	endm

; pattern write/delay/read memory tests
pattern_tests macro
	pattern_write	\1,\2,\3,$0000
	delay_0_5s	\1,2
	pattern_check	\1,\2,\3,$0000
	anti_ptn_write	\1,\2,\3,$0000
	delay_0_5s	\1,2
	anti_ptn_check	\1,\2,\3,$0000
	endm

mem_tests macro
	walking_tests \1,\2,\3
	pattern_tests \1,\2,\3
	endm

; update on-screen pass count
update_count macro
	fcb COUNT
	fdb \1		; video address
	endm

; jump to new sequence point
seq_jump macro
1	fcb SEQJUMP
	fdb \1		; video address
	fdb \2-1b	; jump offset
	endm

; -------------------------------------
; Test sequence entry point
; The sequence is formed from a list of functions and their parameters
; The first parameter is always the video display address
; For test functions, the 2nd and 3rd parameters are start and end addresses
; The pattern check 4th parameter is the pattern seed


seq_start
	rom_check	$0000,seq_rom
	check_64k_ram	$0000,seq_ram_64

; 32K sequence keeps video in lower 16K
; (some 32K machines can't have a display in the upper 16K)
seq_ram_32
	mem_tests	$0000,$3e00,$7fff
	relocate_all	$0000,$3e00,$4000
	mem_tests	$3e00,$0000,$3dff
	relocate_all	$3e00,$0000,$0200
	update_count	$0000
	seq_jump	$0000,seq_ram_32


; RAM-resident sequence jumps here to test 64K

seq_ram_64
	mem_tests	$0000,$4000,$feff
	relocate_all	$0000,$4000,$6000
	mem_tests	$4000,$0000,$3fff
	relocate_all	$4000,$0000,$0200
	update_count	$0000
	seq_jump	$0000,seq_ram_64


; Sequence jumps here if running from ROM

seq_rom
	check_64k_rom	$0000,seq_rom_64

; 32K sequence keeps video in lower 16K
; (some 32K machines can't have a display in the upper 16K)
seq_rom_32
	mem_tests	$0000,$3e00,$7fff
	relocate_vid	$0000,$3e00
	mem_tests	$3e00,$0000,$3dff
	update_count	$3e00
	relocate_vid	$3e00,$0000

	; larger pattern checks cover more addressing faults
	pattern_tests	$0000,$0200,$7fff,$0000

	seq_jump	$0000,seq_rom_32


; ROM-resident sequence jumps here to test 64K

seq_rom_64
	walking_tests	$0000,$4000,$7fff
	relocate_vid	$0000,$4000
	walking_tests	$4000,$0000,$3fff
	relocate_vid	$4000,$0000

	set_page1	$0000

	walking_tests	$0000,$4000,$7fff
	relocate_vid	$0000,$4000
	walking_tests	$4000,$0000,$3fff
	relocate_vid	$4000,$0000

	; run pattern writes across both 32K pages before checking
	; this should cover more addressing faults
	; note different seeds used for the lower and upper 32K pages

	pattern_write	$0000,$0200,$7fff,$8000
	set_page0	$0000
	pattern_write	$0000,$0200,$7fff,$0000
	delay_0_5s	$0000,2
	set_page1	$0000
	pattern_check	$0000,$0200,$7fff,$8000
	set_page0	$0000
	pattern_check	$0000,$0200,$7fff,$0000

	set_page1	$0000
	anti_ptn_write	$0000,$0200,$7fff,$8000
	set_page0	$0000
	anti_ptn_write	$0000,$0200,$7fff,$0000
	delay_0_5s	$0000,2
	set_page1	$0000
	anti_ptn_check	$0000,$0200,$7fff,$8000
	set_page0	$0000
	anti_ptn_check	$0000,$0200,$7fff,$0000

	update_count	$0000

	seq_jump	$0000,seq_rom_64


end_address

	; pad image for rom
	align 256,0


