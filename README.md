## memtest

Memory test utility for Dragon 32 and 64 computers

- Walking bit and pattern tests
- Runs continuously until failure, displaying a pass counter on screen
- Stops on failure, showing the failed address and data values
- Self-relocating program code and video display allows all memory to be tested
- Can be run from RAM or cartridge ROM
- Automatic selection of test script depending on configuration (32K/64K/RAM/ROM)
- Doesn't use memory for variables or stack (Other than video display)

### How to use

To load the wav file via the cassette port, connect the Dragon cassette input cable to your playback device and type the following command on the Dragon followed by the enter key:

```
CLOADM
```

Then start playback. The volume may need to be turned up fairly high. The software will automatically run when it has finished loading.

Alternatively burn the rom file to an EPROM and install in an autostart cartridge.

The screen may occasionally flicker during running. This is normal and is due to screen updates not being synchronised with vertical blanking, minimising dependencies on functioning hardware.

### Assembling from source

#### Prerequisites

- [asm6809 v2.12 or later](https://www.6809.org.uk/asm6809/) - 6809 assembler
- [bin2cas](https://www.6809.org.uk/dragon/#cas2wav) - for converting Dragon binary image to .cas or .wav cassette format
- [dzip](https://www.6809.org.uk/dragon/#dzip) - compression utility to speed up cassette loading time

##### Build a Dragon cartridge ROM image

```
$ cd memtest
$ make memtest.rom
```

Alternatively:

```
$ cd memtest
$ asm6809 -o memtest.rom memtest.s
```

##### Build a wav audio file for loading via the Dragon cassette port

```
$ cd memtest
$ make memtest.wav
```

Alternatively:

```
$ cd memtest
$ asm6809 -D -o memtest.bin memtest.s
$ bin2cas.pl --autorun -z --fast -r 22050 -o memtest.wav -D memtest.bin
```

### Source code notes

The structure is a little unconventional in that the stack is not used and there are no variables. All program state is held in registers. This allows a ROM-based memory test to proceed in a deterministic way even with bad memory.

Subroutine calls are achieved with a branch and link construct, which saves the program counter in a register before jumping to a subroutine.

Position independent code is used so that the program can relocate itself to a different region of memory and increase test coverage.

The memory test scripts are built from lists of function calls and associated parameters, and can be found towards the end of the source code file.

The ROM-based 64K RAM test sequence works by using the SAM page select bit to access 32K at a time. This entails some messing around with the video display, and the method used here restricts the video base address to the lower 32K.

Note that due to hardware limitations of some Dragon 32K variants, the video screen should be placed somewhere in the first 16K of memory.

### License

[GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)

### snacknowledgements

- Fig rolls with peanut butter. Ooh you would.
